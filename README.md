## Setup

```
git clone https://kwasniew@bitbucket.org/kwasniew/tdd_greenfield_redux_chat.git
cd tdd_greenfield_redux_chat
npm i
npm start
npm test
```

This is Redux-enabled version of the tdd_greenfield_react_app

What's changed in production code?
* boot.js has react-redux boilerplate that injects Redux store
to all child components 
* App.js is no longer a class component as we don't need to pass store
down the component hierarchy
* CreateMessage.js is wrapped in a connect component and we export connected
and unconnected versions
* Message.js is wrapped in a connect component and we export connected and
unconnected versions
* CreateMessage.js sends messages to Redux store but keeps it's local state
for the input field changes tracking

What's change in tests?
* App.integration.test.js uses boot.js to bootstrap the app
* CreateMessage.test.js uses name exported class instead of default export
* Messages.test.js uses name exported class instead of default export


Topics covered:
* Redux
* Reducers
* Actions
* Connected components
* Async Redux