import ReactDOM from 'react-dom';
import './App.css';
import boot from './boot';

ReactDOM.render(boot(), document.getElementById('root'));
