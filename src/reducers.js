const CREATE_MESSAGE = 'CREATE_MESSAGE';

const INITIAL_STATE = {
    messages: []
};

// reducer
export default function reducer(state = INITIAL_STATE, action = {}){
    switch(action.type) {
        case CREATE_MESSAGE:
            return {
                ...state,
                messages: [action.message, ...state.messages]
            };
        default:
            return state;
    }
}

// action creator
export function createMessage(message){
    return {
        type: CREATE_MESSAGE,
        message: message
    };
}


// reducer + action creator in one file are easier to manager