import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import reducers from './reducers'
import App from './App';

export default function createReduxedApp(customMiddleware){
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    let middleware = [thunk];
    if(customMiddleware) {
        middleware = [customMiddleware, ...middleware];
    }
    const store = createStore(reducers, composeEnhancers(applyMiddleware(...middleware)));

    return (
        <Provider store={store}>
            <App />
        </Provider>
    );
}