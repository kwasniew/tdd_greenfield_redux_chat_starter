import React from 'react';
import Messages from "./Messages";
import CreateMessage from "./CreateMessage";

export default function App() {
    return (
        <div>
            <Messages/>
            <CreateMessage/>
        </div>
    );
}
