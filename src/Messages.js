import React from 'react';
import {connect} from 'react-redux';

const mapStateToProps = state => ({ messages: state.messages });

export function Messages({messages = []} = {}) {
    if(!messages || messages.length === 0) {
        return null;
    }

    return (
        <div className="messages">
            <h1 className="messages__title">Messages</h1>
            <ul>
                {messages.map((m, i) => <li key={i} className="messages__message">{m}</li>)}
            </ul>
        </div>
    );
}

export default connect(mapStateToProps, null)(Messages);