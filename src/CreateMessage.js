import React from 'react';
import {connect} from 'react-redux';
import {createMessage} from './reducers';

const mapDispatchToProps = {
    onMessage: createMessage
};

export class CreateMessage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message: ''
        };

        this.inputChange = this.inputChange.bind(this);
        this.messageSubmit = this.messageSubmit.bind(this);
    }

    inputChange(event) {
        const message = event.target.value;
        this.setState({message});
    }

    messageSubmit(event) {
        event.preventDefault();

        this.props.onMessage && this.props.onMessage(this.state.message);

        this.setState({
            message: ''
        });
    }

    render() {
        return (
            <form className="create-message" onSubmit={this.messageSubmit}>
                <input type="text" onChange={this.inputChange} className="create-message__input" value={this.state.message} placeholder="create a message"/>
                <input
                    type="submit"
                    className="create-message__submit"
                    value="Send"
                />
            </form>
        );
    }
}

export default connect(null, mapDispatchToProps)(CreateMessage);