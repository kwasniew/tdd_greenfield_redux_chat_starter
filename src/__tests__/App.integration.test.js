import React from "react";
import {mount} from "enzyme";

import createReduxedApp from "../boot";

test("initially no messages", () => {
    const {messages} = setup();

    expect(messages()).toEqual([]);
});

test("new message appears on the list", () => {
    const {messages, inputMessage, submit} = setup();

    inputMessage("first");
    submit();

    expect(messages()).toEqual(["first"]);
});

test("multiple messages appears on the list in reverse order", () => {
    const {messages, inputMessage, submit} = setup();

    inputMessage("first");
    submit();
    inputMessage("second");
    submit();

    expect(messages()).toEqual(["second", "first"]);
});

function setup() {
    const app = mount(createReduxedApp());

    function messages() {
        return app.find(".messages__message").map(m => m.text());
    }

    function inputMessage(msg) {
        app.find(".create-message__input").simulate("change", {target: {value: msg}});
    }

    function submit() {
        app.find(".create-message__submit").simulate("submit"); // here we don't have to work on a form as in unit test with shallow
    }

    return {messages, inputMessage, submit};

}