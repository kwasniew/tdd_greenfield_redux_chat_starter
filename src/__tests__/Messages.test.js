import React from 'react';
import {shallow} from "enzyme";

import {Messages} from '../Messages';

test('Messages has correct title', () => {
    const component = shallow(<Messages messages={["something"]}/>);

    expect(component.find('.messages__title')).toHaveText('Messages');
});

test("Shows list of messages", () => {
    const component = shallow(<Messages messages={["first", "second"]}/>)

    const messages = component.find(".messages__message");

    expect(messages.length).toBe(2);
    expect(messages.at(0)).toHaveText("first");
    expect(messages.at(1)).toHaveText("second");
});

test("Hides message area when no messages", () => {
    const component = shallow(<Messages messages={null}/>);

    const messages = component.find(".messages__message");

    expect(messages.find(".messages")).not.toBePresent();
});