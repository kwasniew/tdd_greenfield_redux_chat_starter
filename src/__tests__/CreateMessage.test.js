import React from 'react';
import {shallow} from "enzyme";

import {CreateMessage} from '../CreateMessage';

function setup({onMessage} = {}) {
    const component = shallow(<CreateMessage onMessage={onMessage}/>);
    const messageInput = messageInputExtractor(component);
    const simulateInputText = inputTextSimulator(messageInput);
    const simulateSubmit = submitFormSimulator(component);

    return {component, messageInput, simulateInputText, simulateSubmit};
}

test('Starts with an empty message', () => {
    const {messageInput} = setup();

    expect(messageInput()).toHaveValue('');
});

test("Have a correct placeholder", () => {
    const {messageInput} = setup();

    expect(messageInput()).toHaveProp('placeholder', 'create a message');
});

test("Update text when typing", () => {
    const {messageInput, simulateInputText} = setup();

    simulateInputText("hi there");

    expect(messageInput()).toHaveValue("hi there");
});

test("Submit message when button is clicked", () => {
    function submitMessageHandler(message) {
        submitMessageHandler.invokedWith = message;
    }
    const {simulateSubmit, simulateInputText} = setup({onMessage: submitMessageHandler});

    simulateInputText("hi there");
    const event = simulateSubmit();

    expect(submitMessageHandler.invokedWith).toBe("hi there");
    expect(event.preventDefault.invoked).toBe(true);
});

test("Submit message when button is clicked [using jest mock fns]", () => {
    const submitMessageHandler = jest.fn();

    const component = shallow(<CreateMessage onMessage={submitMessageHandler}/>);
    const messageInput = messageInputExtractor(component);
    const simulateSubmit = submitFormSimulatorAlt(component);
    const simulateInputText = inputTextSimulator(messageInput);

    simulateInputText("hi there");
    const event = simulateSubmit();

    expect(submitMessageHandler).toHaveBeenCalledWith("hi there");
    expect(event.preventDefault).toHaveBeenCalled();
});

test("Reset text after submit", () => {
    const {messageInput, simulateSubmit, simulateInputText} = setup();

    simulateInputText("some input");
    simulateSubmit();

    expect(messageInput()).toHaveValue("");
});

function submitFormSimulatorAlt(component) {
    return function simulateSubmit() {
        const event = {
            preventDefault: jest.fn()
        };
        component.find("form").simulate("submit", event);
        return event;
    }
}

function submitFormSimulator(component) {
    return function simulateSubmit() {
        const event = {
            preventDefault() {
                event.preventDefault.invoked = true;
            }
        };
        component.find("form").simulate("submit", event);
        return event;
    }
}

function messageInputExtractor(component) {
    return function() {
        return component.find('.create-message__input');
    };
}

function inputTextSimulator(messageInput) {
    return function simulateInputText(text) {
        return messageInput().simulate('change', {target: {value: text}});
    }
}